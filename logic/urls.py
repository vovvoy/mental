from django.urls import path
from .view import LogicView

urlpatterns = [
    path('', LogicView.as_view(), name='logic'),
]